#pragma once
#include <fstream>
namespace Utils {


size_t countLines(std::fstream & file);

} //namespaceUtils
