#include "LineFilestream.hpp"
#include <algorithm>
#include <iostream>

void LineFilestream::toStart() {
    seekg(0);
    clear();
}

void LineFilestream::addLine(const std::string & line) {
    write(line.data(), line.size());
    write("\n", 1);
    flush();
    _linesCounter++;
    if (!good()) {
        std::cout << "warning on write\n";
    }
}

void LineFilestream::resetLines() {
    toStart();
    _linesCounter = 0;
    _readedCounter = 0;
}

void LineFilestream::skipLines(size_t count) {
    for (size_t i = 0; i < count; i++) {
        std::string _;
        std::getline(*this, _);
    }
    clear();
}

bool LineFilestream::isEnd() {
    if (_readedCounter >= _linesCounter) {
        return true;
    } else {
        return false;
    }
}

std::string LineFilestream::getLine() {
    std::string line;
    if (!*this) {
        std::cout << "bad before getLine" << std::endl;
    }

    std::getline(*this, line);
    if (!*this) {
        if (fail()) {
            std::cout << "fail in getLine" << std::endl;
        }
        if (bad()) {
            std::cout << "bad in getLine" << std::endl;
        }
    }

    clear();
    _readedCounter++;
    return line;

}

