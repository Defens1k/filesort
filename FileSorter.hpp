#pragma once
#include <fstream>
#include "LineFilestream.hpp"

class FileSorter {
public:
    FileSorter(const std::string & filename);
    ~FileSorter();
    void sort();
    static bool comparator(const std::string & a, const std::string & b) {
        return a < b;
    }
protected:

    void sortWithSliceSize(size_t sliceSize);

    void sortSlice(const size_t sliceStartLine, size_t sliceSize);






    LineFilestream _sortingFile;
    LineFilestream _tmpFile;


    std::string _tmpFilePath;

    size_t _totalLines = 0;
};
