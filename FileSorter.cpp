#include "FileSorter.hpp"
#include "utils.hpp"
#include <ctime>
#include <iostream>
#include <filesystem>

FileSorter::FileSorter(const std::string &filename) {
    std::srand(std::time(0));
    _tmpFilePath = "/tmp/filesort" + filename + std::to_string( std::rand());

    if (!std::filesystem::exists(filename)) {
        throw std::runtime_error("No such file " + filename);
    }

    _sortingFile.open(filename, std::ios_base::out | std::ios_base::in | std::ios_base::binary);
    if (!_sortingFile.is_open()) {
        throw std::runtime_error("Can not open file " + filename + " for read/write mode");
    }
    std::ofstream creatingTmpFile(_tmpFilePath);
    creatingTmpFile.close();
    _tmpFile.open(_tmpFilePath, std::ios_base::out | std::ios_base::in | std::ios_base::binary);
    if (!_tmpFile.is_open()) {
        throw std::runtime_error("Can not open file " + _tmpFilePath + " for read/write mode");
    }
    _totalLines = Utils::countLines(_sortingFile);
}

FileSorter::~FileSorter() {
    _tmpFile.close();
    std::filesystem::remove(_tmpFilePath);
}

void FileSorter::sort() {
    if (!_sortingFile) {
        std::cout << "bad before start sort" << std::endl;
    }

    size_t sliceSize = 2;
    while (1) {
        if (sliceSize / 2 > _totalLines) {
            break;
        }

        sortWithSliceSize(sliceSize);
        sliceSize *= 2;

    }




}

void FileSorter::sortWithSliceSize(size_t sliceSize) {
    _sortingFile.toStart();

    size_t sliceStartLine = 0;
    while (1) {
        if (sliceStartLine >= _totalLines - 1) {
            break;
        }
        sortSlice(sliceStartLine, sliceSize);
        _sortingFile.flush();

        sliceStartLine += sliceSize;
    }

}

void FileSorter::sortSlice(const size_t sliceStartLine, size_t sliceSize) {
    _tmpFile.resetLines();

    const size_t sliceMedianPos = sliceStartLine + sliceSize / 2;
    const size_t sliceEndPos = std::min(_totalLines, sliceStartLine + sliceSize);
    size_t sliceRightLinePos = sliceMedianPos;
    size_t sliceLeftLinePos = sliceStartLine;
    if (sliceEndPos <= sliceMedianPos) {
        return;
    }
    auto sliceStartRaw = _sortingFile.tellg();

    auto leftSubslicePos = sliceStartRaw;
    if (!_sortingFile) {
        std::cout << "bad before skip Lines" << std::endl;
    }

    _sortingFile.skipLines(sliceSize / 2);

    auto rightSubslicePos = _sortingFile.tellg();

    std::string leftLineBuffer, rightLineBuffer;

    if (!_sortingFile) {
        std::cout << "bad before seekg" << std::endl;
    }

    _sortingFile.seekg(rightSubslicePos);
    rightLineBuffer = _sortingFile.getLine();

    if (!_sortingFile) {
        std::cout << "bad" << std::endl;
    }

    rightSubslicePos = _sortingFile.tellg();
    sliceRightLinePos++;

    _sortingFile.seekg(leftSubslicePos);
    leftLineBuffer = _sortingFile.getLine();
    leftSubslicePos = _sortingFile.tellg();
    sliceLeftLinePos++;

    while (1) {

        if (sliceLeftLinePos >= sliceMedianPos) {
            break;
        }

        if (sliceRightLinePos >= sliceEndPos) {
            break;
        }

        if (comparator(leftLineBuffer, rightLineBuffer)) {
            _tmpFile.addLine(leftLineBuffer);

            _sortingFile.seekg(leftSubslicePos);
            leftLineBuffer = _sortingFile.getLine();
            leftSubslicePos = _sortingFile.tellg();
            sliceLeftLinePos++;



        } else {
            _tmpFile.addLine(rightLineBuffer);

            _sortingFile.seekg(rightSubslicePos);
            rightLineBuffer = _sortingFile.getLine();
            rightSubslicePos = _sortingFile.tellg();
            sliceRightLinePos++;

        }
    }
    auto nextLine = std::min(leftLineBuffer, rightLineBuffer, comparator);
    auto unusedLine = std::max(leftLineBuffer, rightLineBuffer, comparator);
    bool isUseLine = false;
    _tmpFile.addLine(nextLine);


    if (sliceLeftLinePos < sliceMedianPos) {
        while (1) {

             if (sliceLeftLinePos >= sliceMedianPos) {
                break;
            }

            _sortingFile.seekg(leftSubslicePos);
            leftLineBuffer = _sortingFile.getLine();
            leftSubslicePos = _sortingFile.tellg();
            sliceLeftLinePos++;

            if (!isUseLine and comparator(unusedLine, leftLineBuffer)) {
                _tmpFile.addLine(unusedLine);
                isUseLine = true;
            }
            _tmpFile.addLine(leftLineBuffer);
        }
    }


    if (sliceRightLinePos < sliceEndPos) {
        while (1) {

             if (sliceRightLinePos >= sliceEndPos) {
                break;
            }

            _sortingFile.seekg(rightSubslicePos);
            rightLineBuffer = _sortingFile.getLine();
            rightSubslicePos = _sortingFile.tellg();
            sliceRightLinePos++;


            if (!isUseLine and comparator(unusedLine, rightLineBuffer)) {
                _tmpFile.addLine(unusedLine);
                isUseLine = true;
            }
            _tmpFile.addLine(rightLineBuffer);
        }
    }
    if (!isUseLine) {
        _tmpFile.addLine(unusedLine);
        isUseLine = true;
    }


    //copy tmp to sorting


    _sortingFile.seekg(sliceStartRaw);
    _tmpFile.toStart();


    while (!_tmpFile.isEnd()) {
        std::string line = _tmpFile.getLine();
        line.push_back('\n');
        _sortingFile.write(line.data(), line.size());
    }

}








































