#include <fstream>

class LineFilestream : public std::fstream{
public:
    void toStart();
    void addLine(const std::string & line);
    void resetLines();
    void skipLines(size_t count);
    bool isEnd();
    std::string readAndGet();
    std::string getLine();

private:
    size_t _linesCounter = 0;
    size_t _readedCounter = 0;
};

