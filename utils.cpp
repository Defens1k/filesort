#include "utils.hpp"

namespace Utils {

size_t countLines(std::fstream &file) {
    file.seekg(0);
    std::string line;
    size_t counter = 0;
    while (std::getline(file, line)) {
        counter++;
    }
    file.seekg(0);
    file.clear();
    return counter;
}

} //namespace Utils
