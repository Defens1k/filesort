import sys
import os
import random
import string

def randStr(chars = string.ascii_letters + string.digits, N=10):
	return ''.join(random.choice(chars) for _ in range(N))

f = open(sys.argv[1], "w")

for i in range (int(sys.argv[3])):
    f.write(randStr(N=random.randint(1, int(sys.argv[2]))) + '\n');
