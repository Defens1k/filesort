#include <iostream>
#include "FileSorter.hpp"

int main() {
    std::string fileName = "f.txt";
    std::cout << "Write filename\n";
    std::cin >> fileName;

    FileSorter fileSorter(fileName);
    fileSorter.sort();
}
